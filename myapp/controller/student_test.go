package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddStudent(t *testing.T) {

	// REQUEST
	url := "http://localhost:8080/student"
	var jsonStr = []byte(`{"stdid":123451212,"fname":"Kinley Norbu","lname":"Thinley","email":"ryoutamikasasqw12@gmailcom"}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// REQUESTER
	client := &http.Client{}

	// RESPON
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status": "student added"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestGetStudent(t *testing.T) {

	// REQUESTER
	client := &http.Client{}

	// REQUEST
	res, _ := client.Get("http://localhost:8080/student/12220023")
	body, _ := io.ReadAll(res.Body)

	assert.Equal(t, http.StatusOK, res.StatusCode)
	expResp := `{"email":"ryoutamikasas@gmail.com", "fname":"Kinley Norbu", "lname":"Thinley ", "stdid":12220023}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteStudent(t *testing.T) {

	// REQUEST
	url := "http://localhost:8080/student/12220023"

	req, _ := http.NewRequest("DELETE", url, nil)

	// REQUESTER
	client := &http.Client{}

	// RESPON
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"deleted"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestStudentNotFound(t *testing.T) {

	assert := assert.New(t)

	c := http.Client{}

	r, _ := c.Get("http://localhost:8080/student/10025")

	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"student not found"}`
	assert.JSONEq(expResp, string(body))
}
