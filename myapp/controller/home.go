package controller

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// define the handler function
// compulsory for any handler to receive 2 arguments i.e http.responseWriter and *http.Request(pointer which points to the request type)
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	//Write the response to the client

	//write function returns int (which counts the letters in byte slice) and error
	// using underscore to ignore int type and only going with error type
	_, err := w.Write([]byte("Hello World"))
	if err != nil {
		fmt.Println("error:", err)
	}

}

// handler function for the paramater url
// for the name
func ParameterHandler(w http.ResponseWriter, r *http.Request) {
	p := mux.Vars(r)
	fmt.Println(p)
	name := p["myname"]
	_, err := w.Write([]byte("My name is " + name))
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
}
