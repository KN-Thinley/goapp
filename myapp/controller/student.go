package controller

import (
	"database/sql"
	"encoding/json"
	"mygo/myapp/model"
	"mygo/myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w,r){
		return
	}
	//create variable type Student
	var stud model.Student

	//read the request body and create a decoder object
	decoder := json.NewDecoder(r.Body)

	// store the json object data student variable
	if err := decoder.Decode(&stud); err != nil {

		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")

		// // w.Write([]byte("Invalid json data"))//returns content type plain text
		// map1 := map[string]string{"error": err.Error()} //since this condition is if error is found therefore, we go for error detection in the map
		// jsonResp, _ := json.Marshal(map1)

		// //status code for bad request(400) and 200 for successful
		// w.WriteHeader(http.StatusBadRequest)

		// //response header
		// w.Header().Set("Content-Type", "application/json")
		// // response writer/ response body
		// w.Write(jsonResp)

	}

	// defer the closing of request body until the function returns
	defer r.Body.Close()

	//call the Create() using student object, stud
	saveErr := stud.Create()
	if saveErr != nil {

		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		// // w.Write([]byte("Invalid json data"))//returns content type plain text
		// map1 := map[string]string{"error": saveErr.Error()} //since this condition is if error is found therefore, we go for error detection in the map
		// jsonResp, _ := json.Marshal(map1)

		// //status code for bad request(400) and 200 for successful
		// w.WriteHeader(http.StatusBadRequest)

		// //response header
		// w.Header().Set("Content-Type", "application/json")

		// //response writer/ response body
		// w.Write(jsonResp)

		// w.Write([]byte("Database Error " + saveErr.Error()))
		return
	} else {

		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "student added"})

		// map1 := map[string]string{"message": "sudent data added"}
		// jsonResp, _ := json.Marshal(map1)

		// //set response header
		// w.Header().Set("Content-Type", "application/json")
		// w.WriteHeader(http.StatusCreated)

		// //setting response key
		// w.Write(jsonResp)

	}

	// fmt.Println(stud)

	//Fprintf looks at the format of the var, here w is response writer so it will no display in the terminal but at the browser
	//fmt.Fprintf(w, "add student handler")
}

// helper function to return string to int
func getUserId(sid string) int64 {
	stdid, _ := strconv.ParseInt(sid, 10, 64)
	return stdid
}

func GetStud(w http.ResponseWriter, r *http.Request) {
	// ssid is of type string
	ssid := mux.Vars(r)["sid"]

	//stdid is of type int
	stdid := getUserId(ssid)

	stud := model.Student{StdId: stdid}

	getErr := stud.Read()
	if getErr != nil {
		// httpResp.RespondWithError(w, http.StatusBadRequest, "Student not found")

		switch getErr{
		case sql.ErrNoRows:
				 httpResp.RespondWithError(w, http.StatusNotFound,"student not found")
			
			default:
				httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
	
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
	}

}

func UpdateStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return
	}
	old_sid := mux.Vars(r)["sid"]
	old_stdId := getUserId(old_sid)
	var stud model.Student

	updateErr := stud.Update(old_stdId)
	// idErr := getUserId(old_sid)
	// var idErr error
	if updateErr != nil{
		switch updateErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Student not found" )
		
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
		 
	}
	
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&stud);err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json ")
		return
	}
	defer r.Body.Close()

	err := stud.Update(old_stdId)

	if err != nil{
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, stud)


}

func DeleteStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return
	}
	sid := mux.Vars(r)["sid"]
	stdId := getUserId(sid)
	var idErr error 
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	s := model.Student{StdId: stdId}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status":"deleted"})

}

func GetAllStuds(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return
	}
	students, getErr := model.GetAllStudents()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, students)

}


