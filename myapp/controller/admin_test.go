package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)
func TestAdmLogin(t *testing.T) {
url := "http://localhost:8080/login"
// data of type byte slice
var jsonStr = []byte(`{"Email":"hotsukisatowa5@gmail.com", "Password":"Kinleynorbu5"}`)
// create http request
req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
// set request header
req.Header.Set("Content-Type", "application/json")
// create a pointer variable client which points to Client type
client := &http.Client{}
// client sends http request using Do() and gets http response
resp, err := client.Do(req)
// handle error if any
if err != nil {
panic(err)
}
// defer the closing of response body until function terminates
defer resp.Body.Close()
// get data from the response body
body, _ := io.ReadAll(resp.Body)
// validate if response status is same as expected status code
assert.Equal(t, http.StatusOK, resp.StatusCode)
expResp := `{"message":"success"}`
// validate if response body is same as expected response body
assert.JSONEq(t, expResp, string(body))
}

//test to check if the test file for showing error also works
func TestAdmLoginDoesNotExist(t *testing.T){
	//data of type byte slice
	var jsonStr = []byte(`{"Email":"hotsuki@gmail.com","Password":"Kinleynorbu5"}`)

	//create request object
	req, _ := http.NewRequest("POST", "http://localhost:8080/login", bytes.NewBuffer(jsonStr))

	//set request header 
	req.Header.Set("Content-Type", "application/json")

	//create a pointer variable client which point to Client type 
	client := &http.Client{}

	//client sends http request using Do() and gets http response
	resp, err := client.Do(req)

	//handle error if any
	if err != nil {
		panic(err)
	}
	//defer is  a built in keyword (shifting) 
	//this help skip the particular line 
	//defer the closing of response body until function terminates 
	defer resp.Body.Close()

	//get data from the response body
	body, _ := io.ReadAll(resp.Body)

	//validate if response status is same as expected status cdoe 
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
	expResp :=`{"error": "sql: no rows in result set"}`

	//validate if response body is same as expected reponse body
	assert.JSONEq(t, expResp, string(body))
}

