package controller

import (
	"encoding/json"
	"mygo/myapp/model"
	"mygo/myapp/utils/httpResp"
	"net/http"
	"time"
)

//for the sign up page
func Signup(w http.ResponseWriter, r *http.Request){
	var admin model.Admin 

	// convert the json data to go object (struct type)
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&admin); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr := admin.Create()
	
	if saveErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
	}
	//no error 
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})

}

//For the login page 
func Login(w http.ResponseWriter, r *http.Request){
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}else{
		//create cookie 
	cookie := http.Cookie{
		Name: "my-cookie",
		Value: "my-value", //user-details are usually set here
		Expires: time.Now().Add(20*time.Minute),
		Secure: true,
	}
	http.SetCookie(w, &cookie)
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "success"})
	}

	
	
}

func Logout (w http.ResponseWriter, r *http.Request){
	http.SetCookie(w, &http.Cookie{
		Name: "my-cookie",
		Expires: time.Now(),
	})
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "logout successful"})
}

// Function for the cookies 
func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	//check cookie in request
	cookie, err := r.Cookie("my-cookie")

	//handle error

	if err != nil {
		if err == http.ErrNoCookie{
			httpResp.RespondWithError(w, http.StatusUnauthorized, "cookie not found")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Internal server error ")
		return false
	}

	if cookie.Value != "my-value" {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "cookie value doesn't match ")
		return false
	}
	return true



}