window.onload = function () {
    //get all the student id
    fetch('/students')
    .then(response => response.text())
    .then(data => getStudents(data))

    //get all the courses
    fetch('/courses')
    .then(response => response.text())
    .then(data =>  getCourses(data))

    //get all the enrolls 
    fetch('/enrolls')
    .then(response => response.text())
    .then(data => getAllEnroll(data))
}


function getStudents(data){
    const students = []// to store all student ID
    const allStudents = JSON.parse(data)

    //add student id to students list 
    allStudents.forEach(stud => {
        students.push(stud.stdid)
    });

    var select = document.getElementById("sid")

    for (var i = 0; i < students.length; i ++ ){
        var sid = students[i];
        var option = document.createElement("option")
        option.textContent = sid;
        option.value = sid ;
        select.appendChild(option)
    }
}

function getCourses(data){
    const courses = []
    const allCourses = JSON.parse(data)

    allCourses.forEach(course => {
        courses.push(course.cid)
    });

    var option = "";
    for ( var i = 0; i < courses.length; i++){
        option += '<option value="'+ courses[i] +'">' +courses[i]+"</option>"
    }
    document.getElementById("cid").innerHTML = option;
}

// function addEnroll(){
//     var _data = {
//         stdid : parseInt(document.getElementById("sid").value),
//         cid: document.getElementById("cid").value,
//     }
//     var sid = _data.stdid
//     var cid = _data.cid

//     if (isNaN(sid) || cid == ""){
//         alert("Select valid data")
//         return
//     }
//     fetch('/enroll', {
//         method: "POST",
//         body: JSON.stringify(_data),
//         headers: {"Content-type": "application/json; charset=UTF-*"}
//     }).then(response => {
//         if (response.ok){
//             fetch("/enroll/" + sid + "/"+cid)
//             .then(response => response.text())
//             .then(data => getEnrolled(data))
//         }else{
//             throw new Error(response.statusText)
//         }
//     }).catch(e => {
//         if (e == "Erro: Forbidden"){
//             alert(e+ ". Duplicate entry")
//         }
//     })
// }

// function getEnrolled(data){
//     const enrolled = JSON.parse(data)
//     showTable(enrolled)
// }

// function showTable(enrolled) {
//     // Find a <table> element with id="myTable":
//     var table = document.getElementById("myTable");
//     // Create an empty <tr> element and add it to the last position of the table:
//     var row = table.insertRow(table.length);
//     // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
//     var td=[]
//     for(i=0; i<table.rows[0].cells.length; i++){
//     td[i] = row.insertCell(i);
//     }
//     td[0].innerHTML = enrolled.stdid;
//     td[1].innerHTML = enrolled.cid;
//     td[2].innerHTML = enrolled.date.split("T")[0]; // show only date,ignore time
//     td[3].innerHTML = '<input type="button" onclick="deleteEnroll(this)" value="Delete" id="button-1">';
// }

function addEnroll() {
    var _data = {
        stdid: parseInt(document.getElementById("sid").value),
        cid: document.getElementById("cid").value,
    };

    var sid = _data.stdid;
    var cid = _data.cid;

    if (isNaN(sid) || cid === "") {
        alert("Select valid data");
        return;
    }

    fetch("/enroll", {
        method: "POST",
        body: JSON.stringify(_data),
        headers: { "Content-type": "application/json; charset=UTF-8" }, // Corrected charset value
    })
        .then(function(response) {
            if (response.ok) {
                return fetch("/enroll/" + sid + "/" + cid); // Corrected URL
            } else {
                throw new Error(response.statusText);
            }
        })
        .then(function(response) {
            return response.text();
        })
        .then(function(data) {
            getEnrolled(data);
        })
        .catch(function(e) {
            if (e.message === "Forbidden") { // Changed comparison to e.message
                alert(e.message + ". Duplicate entry");
            }
        });
}

function getEnrolled(data) {
    const enrolled = JSON.parse(data);
    showTable(enrolled);
}

function showTable(enrolled) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.rows.length); // Corrected table.length to table.rows.length
    var td = [];

    for (i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = enrolled.stdid;
    td[1].innerHTML = enrolled.cid;
    td[2].innerHTML = enrolled.date.split("T")[0];
    td[3].innerHTML = '<input type="button" onclick="deleteEnroll(this)" value="Delete" id="button-1">';
}



function getAllEnroll(data){
    const allenroll = JSON.parse(data)
    allenroll.forEach(enroll => {
        showTable(enroll)
    });
}

// const deleteEnroll = async(r) => {
//     if(confirm("Are you sure you want to DELETE this? ")){
//         selectedRow = r.parentElement.parentElement;
//         sid = selectedRow.cells[0].innerHTML;
//         cid = selectedRow.cells[1].innerHTML;

//         fetch("/enroll/"+sid+"/"+cid, {
//             method: "DELETE",
//             headers: {"Content-type": "application/json; charset=UTF-8"}
//         }).then(response => {
//             if (response.ok) {
//                 var rowIndex = selectedRow.rowIndex; //index starts from 0
//                 if (rowIndex>0){
//                     document.getElementById("myTable").deleteRow(rowIndex)
//                 }
//             }
//         })
//     }
// }

const deleteEnroll = async (r) => {
    if (confirm("Are you sure you want to DELETE this?")) {
        selectedRow = r.parentElement.parentElement;
        sid = selectedRow.cells[0].innerHTML;
        cid = selectedRow.cells[1].innerHTML; // Corrected "innterHTML" to "innerHTML"

        fetch("/enroll/" + sid + "/" + cid, {
            method: "DELETE",
            headers: { "Content-type": "application/json; charset=UTF-8" },
        })
            .then((response) => {
                if (response.ok) {
                    var rowIndex = selectedRow.rowIndex; // Index starts from 0
                    if (rowIndex > 0) {
                        document.getElementById("myTable").deleteRow(rowIndex);
                    }
                }
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    }
};
