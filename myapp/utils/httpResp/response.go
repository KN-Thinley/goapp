package httpResp

import (
	"encoding/json"
	"net/http"
)

//writing the whole code in controller function to the separate function
func RespondWithError( w http.ResponseWriter, code int, message string){
	RespondWithJSON(w, code, map[string]string{"error": message})
}
func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}){
	//convert with json response 
	jsonResp, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonResp)
}