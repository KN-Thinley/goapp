package model

import "mygo/myapp/dataStore/postgres"

type Student struct {
	//  alias are used to shorten the fields
	// it is not compulsory but only for convinience

	StdId     int64  `json:"stdid"`
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
}


const queryInsertUser = "INSERT INTO student(stdid, firstname, lastname, email) VALUES($1, $2, $3, $4);"

const queryGetUser = "SELECT stdid, firstname, lastname, email FROM student WHERE stdid=$1"

const queryUpdateUser = "UPDATE student SET stdid=$1, firstname=$2, lastname=$3, email=$4 WHERE stdid=$5 RETURNING stdid"

const queryDeleteUSer = "DELETE FROM student WHERE 	stdid=$1 RETURNING stdid;"

func (s *Student) Create() error {
	_, err := postgres.Db.Exec(queryInsertUser, s.StdId, s.FirstName, s.LastName, s.Email)
	return err
}

func (s *Student) Read() error {
	return postgres.Db.QueryRow(queryGetUser, s.StdId).Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)

}

func (s *Student) Update(oldID int64) error{
row := postgres.Db.QueryRow(queryUpdateUser, s.StdId, s.FirstName, s.LastName, s.Email, oldID)
err := row.Scan(&s.StdId)
return err
}

func (s *Student) Delete() error{
	err := postgres.Db.QueryRow(queryDeleteUSer, s.StdId).Scan(&s.StdId)
	 return err
}

func GetAllStudents() ([]Student, error){
	rows, getErr := postgres.Db.Query("SELECT * from student;")

	if getErr != nil {
		return nil, getErr
	}

	// create a slice of type student

	students := []Student{}

	for rows.Next(){
		var s Student 
		dbErr := rows.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)

		if dbErr != nil{
			return nil, dbErr
		}

		students = append(students,s)

	}
	rows.Close()
	return students, nil
}
