package model

import "mygo/myapp/dataStore/postgres"

type Enroll struct {
	StdId    int64 `json:"stdid"`
	CourseId string `json:"cid"`
	Date     string  `json:"date"`
}

const queryEnrolled = "INSERT INTO enroll(std_id, course_id, date_enrolled) VALUES ($1, $2, $3) RETURNING std_id"
const queryGetEnroll = "SELECT std_id, course_id, date_enrolled FROM enroll WHERE  std_id=$1 and course_id=$2"
const queryDeleteEnroll = "DELETE FROM enroll WHERE std_id=$1 and course_id=$2"

func (e *Enroll) EnrollStudent()error{
	//query row returns one row
	row := postgres.Db.QueryRow(queryEnrolled, e.StdId, e.CourseId, e.Date)
	err := row.Scan(&e.StdId)
	return err
}

func (e *Enroll) Get() error{
	return postgres.Db.QueryRow(queryGetEnroll, e.StdId, e.CourseId).Scan(&e.StdId, &e.CourseId, &e.Date)
}

func GetAllEnrolls() ([]Enroll, error){
	rows, getErr := postgres.Db.Query("SELECT std_id, course_id, date_enrolled from enroll")

	if getErr != nil {
		return nil, getErr
	}

	//create a slice of type course
	enrolls := []Enroll{}

	for rows.Next(){
		var e Enroll
		dbErr := rows.Scan(&e.StdId, &e.CourseId, &e.Date)
		if dbErr != nil {
			return nil, dbErr
		}
		enrolls = append(enrolls, e)
	}
	rows.Close()
	return enrolls, nil 
}

func (e *Enroll) Delete()error{
	if _, err := postgres.Db.Exec(queryDeleteEnroll, e.StdId, e.CourseId); err != nil {
		return err
	}
	return nil
}

