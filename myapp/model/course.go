package model

import "mygo/myapp/dataStore/postgres"


type Course struct {
	Cid string `json:"cid"`
	Coursename string `json:"cname"`
}

// query to insert course into database
const queryInsertCourse = "INSERT INTO course(cid, coursename) VALUES ($1, $2);"

//query to get course from the database
const queryGetCourse = "SELECT * FROM course WHERE cid=$1"

//Query to update course 
const queryUpdateCourse = "UPDATE course SET cid=$1, coursename=$2 WHERE cid=$3 RETURNING cid"

//Query to delete course
const queryDeleteCourse = "DELETE FROM course WHERE cid=$1 RETURNING cid"
//function to add course
func (c *Course) Add() error{
	_, err := postgres.Db.Exec(queryInsertCourse, c.Cid,c.Coursename)
	return err
}

//function to show course 
func (c *Course) Show() error {
	return postgres.Db.QueryRow(queryGetCourse, c.Cid).Scan(&c.Cid, &c.Coursename)
}

//function to update course
func (c *Course) Update(oldCID string) error{
	row := postgres.Db.QueryRow(queryUpdateCourse, c.Cid, c.Coursename, oldCID)
	err := row.Scan(&c.Cid)
	return err
}

// function to delete course 
func (c *Course) Delete() error {
	err := postgres.Db.QueryRow(queryDeleteCourse, c.Cid).Scan(&c.Cid)
	return err
}

func GetAllCourses() ([]Course , error){
	rows, getErr := postgres.Db.Query("SELECT * from course")

	if getErr != nil{
		return nil, getErr
	}

	//create a slice of type course
	courses := []Course{}

	for rows.Next(){
		var c Course
		dbErr := rows.Scan(&c.Cid, &c.Coursename)

		if dbErr != nil {
			return nil, dbErr
		}

		courses = append(courses, c)
	}

	rows.Close()
	return courses, nil
	
}


