package model

import "mygo/myapp/dataStore/postgres"

// Admin struct for sign up and login
type Admin struct{
	Firstname string
	Lastname string
	Email string 
	Password string
}

const queryInsert = "INSERT INTO admin(FirstName, LastName, Email, Password) VALUES ($1, $2, $3, $4) RETURNING Email"
const queryCheckAdmin = "SELECT Email, Password FROM admin WHERE Email=$1 and Password=$2;"

func (adm *Admin) Create() error {

	//Query row executes the sql query
	//for the returning keyword, 'Email', we use Scan function this will either return the email if successful or return error if unsuccessful
	err := postgres.Db.QueryRow(queryInsert, adm.Firstname, adm.Lastname, adm.Email, adm.Password).Scan(&adm.Email)
	return err
} 

// Get function for the admin login 
func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryCheckAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}