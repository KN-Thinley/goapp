package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

// NOTE: MAKE SURE TO USE THE SAME CONFIGURATION FOR THE POSTGRES DATABASE YOU HAVE ACTUALLY INITIALIZED
// MATCH HOST, PORT, USER PASSWORD AND DBNAME WITH THE ACTUAL CONFIGS
// db details
// const are go in built keyword that helps define constants
const (
	postgres_host     = "dpg-chqndtik728ivvud6arg-a.singapore-postgres.render.com"
	postgres_port     = 5432
	postgres_user     = "postgres_admin"
	postgres_password = "uPexkIQJgRXpx0ViikuxwNgjYrpVLrlH"
	postgres_dbname   = "my_db_kiwu"
)

// create pointer variable Db which points to sql driver
var Db *sql.DB

// init() is always called before main()
func init() {
	// Creating the connection string
	//Sprintf formats according to format specifier and returns resulting string
	//%s for string types %d for int types and disable for sslmode
	db_info := fmt.Sprintf("host=%s port=%d user =%s password=%s dbname=%s", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)
	var err error

	// open connection to database
	//sql.Open() function takes two args. driver name and string that tells how to connect to the database
	//returns a pointer to a sql.DB and an error
	Db, err = sql.Open("postgres", db_info)
	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}
