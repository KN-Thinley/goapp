package routes

import (
	"fmt"
	"log"
	"mygo/myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializerRoutes() {
	//creating a new router
	router := mux.NewRouter()

	//regsiter handler function with the mux router
	router.HandleFunc("/home", controller.HomeHandler)

	//define another route for name
	router.HandleFunc("/home/{myname}", controller.ParameterHandler)


	// ROUTER FOR THE STUDENT TABLE
	//router for the student Addstudent function
	router.HandleFunc("/student", controller.AddStudent).Methods("POST")

	// router data for the read function 
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")

	//router for the update function
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")

	// router for the delete function
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")

	// router for the read function to get all the students 
	router.HandleFunc("/students", controller.GetAllStuds)

	// ROUTER FOR THE COURSE TABLE
	//router for the addCourse function in course
	router.HandleFunc("/course", controller.AddCourse).Methods("POST")

	//route for the data for the read function 
	router.HandleFunc("/course/{cid}", controller.GetCourse).Methods("GET")

	//router for the update function 
	router.HandleFunc("/course/{cid}", controller.UpdateCourse).Methods("PUT")

	//router for the delete function 
	router.HandleFunc("/course/{cid}", controller.DeleteCourse).Methods("DELETE")

	//router for the read function to get all the courses
	router.HandleFunc("/courses", controller.ShowAllCourses).Methods("GET")

	

	// ROUTER FOR THE ADMIN TABLE
	//Create router for sign up page 
	router.HandleFunc("/signup", controller.Signup).Methods("POST")

	//Create router for login page 
	router.HandleFunc("/login", controller.Login).Methods("POST")

	//ROUTER FOR THE COURSE TABLE
	router.HandleFunc("/course",controller.AddCourse).Methods("POST")

	//router for the logout button 
	router.HandleFunc("/logout", controller.Logout).Methods("GET")
	

	// ROUTER FOR THE ENROLL TABLE
	router.HandleFunc("/enroll", controller.Enroll).Methods("POST")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll).Methods("GET")
	router.HandleFunc("/enrolls", controller.GetEnrolls).Methods("GET")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.DeleteEnroll).Methods("DELETE")
	
	//CREATE ALL ROUTES BEFORE SERVING THE STATIC FILES
	//http file server 
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)


	

	// start the http server
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	log.Println("Application running on port 8080")

	//OR
	//log.Fatal(http.ListenAndServe(":8080", router))
}
